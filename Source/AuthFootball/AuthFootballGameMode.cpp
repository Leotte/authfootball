// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "AuthFootballGameMode.h"
#include "AuthFootballHUD.h"
#include "AuthFootballCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAuthFootballGameMode::AAuthFootballGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/Player"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AAuthFootballHUD::StaticClass();
}
