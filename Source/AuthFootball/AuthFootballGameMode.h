// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AuthFootballGameMode.generated.h"

UCLASS(minimalapi)
class AAuthFootballGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AAuthFootballGameMode();
};



