// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "Ball.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"

ABall::ABall()
{
	SetReplicateMovement(true);

	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));

	// Set as root component
	RootComponent = CollisionComp;
}

void ABall::Move(FVector direction)
{
	CollisionComp->AddImpulse(direction);
}

void ABall::ApplyCurve(FVector direction)
{
	CollisionComp->AddAngularImpulseInRadians(direction); //TODO test thrust to add curve and use 
}