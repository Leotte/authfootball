// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "AuthFootballCharacter.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/GameplayStatics.h"
//#include "Net/UnrealNetwork.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AAuthFootballCharacter

AAuthFootballCharacter::AAuthFootballCharacter()
{
	bReplicates = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -25.0f, -155.7f));
}

void AAuthFootballCharacter::GetSteamID() {
	const IOnlineSubsystem* OnlineSub = IOnlineSubsystem::Get();
	if (OnlineSub != nullptr) {
		const IOnlineIdentity* identity = OnlineSub->GetIdentityInterface().Get();
		if (identity != nullptr) {
			PlayerID = identity->GetUniquePlayerId(0); //0 for local player
			if (PlayerID && PlayerID->IsValid()) {
				SteamID = PlayerID->ToString();
				return;
			}
			SteamID = FString("N/A");
			return;
		}
	}

	UE_LOG(LogTemp, Warning, TEXT("No online subsystem available"));
}

void AAuthFootballCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
	GetSteamID();	
}

void AAuthFootballCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (ShootBallInput && HoldPowerForce <= HoldPowerLimit) {
		HoldPowerForce += DeltaTime * HoldPowerInputMultiplier;
	}

	if (MovementCooldownDelta < MovementCooldown) {
		MovementCooldownDelta += DeltaTime;
	}

	if (CarryBallInput) {
		CarryBall();
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AAuthFootballCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump event
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);


	// Bind run event
	PlayerInputComponent->BindAction("Running", IE_Pressed, this, &AAuthFootballCharacter::StartRun);
	PlayerInputComponent->BindAction("Running", IE_Released, this, &AAuthFootballCharacter::StopRun);

	// Bind shoot event
	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &AAuthFootballCharacter::HoldPowerUp);
	PlayerInputComponent->BindAction("Shoot", IE_Released, this, &AAuthFootballCharacter::ReleasePowerUp);

	// Bind carry event
	PlayerInputComponent->BindAction("Carry", IE_Pressed, this, &AAuthFootballCharacter::StartCarryBall);
	PlayerInputComponent->BindAction("Carry", IE_Released, this, &AAuthFootballCharacter::StopCarryBall);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AAuthFootballCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AAuthFootballCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AAuthFootballCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AAuthFootballCharacter::LookUpAtRate);
}

void AAuthFootballCharacter::ShootBall()
{
	if (Ball != NULL && CheckForMovementCooldown())
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			float DistanceToBall = FVector().Dist2D(GetActorLocation(), Ball->GetActorLocation());

			if (DistanceToBall > MaxDistanceToBall) {
				UE_LOG(LogTemp, Warning, TEXT("Distance to Ball: %f"), DistanceToBall);
			}
			else {
				FVector ForwardVector = GetActorForwardVector() * (ShootForce + HoldPowerForce);
				FVector UpwardVector = GetActorUpVector() * HoldPowerForce;

				Ball->Move(ForwardVector + UpwardVector);
				Ball->ApplyCurve(FVector(FMath::RandRange(-100.0f, 100.0f), FMath::RandRange(-100.0f, 100.0f), FMath::RandRange(-100.0f, 100.0f)) * HoldPowerForce);
				UE_LOG(LogTemp, Warning, TEXT("HoldPowerForce: %f"), HoldPowerForce);
			}
		}
	}
}

void AAuthFootballCharacter::CarryBall()
{
	if (Ball != NULL && CheckForMovementCooldown())
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			float DistanceToBall = FVector().Dist2D(GetActorLocation(), Ball->GetActorLocation());

			if (DistanceToBall > MaxDistanceToBall) {
				return;
			}

			if (ForwardInput >= 0.5f || Running) { //A faster push on the ball
				FVector UpwardVector = GetActorUpVector() * FMath::Max(0.0f, Velocity.Size() / WalkingSpeed); //needs random upwards movement for realistic bounce
				FVector SidewaysVector = GetActorRightVector() * RightInput;
				FVector ForwardVector = GetActorForwardVector() * FMath::Max(-0.1f, ForwardInput);

				ForwardVector *= Velocity.Size() * RunningVelocityMultiplier;
				SidewaysVector *= RunningVelocityMultiplier * 400.0f + ForwardVector.Size()/15;

				Ball->Move(ForwardVector + SidewaysVector + UpwardVector);				
			}
			else { //A slower carrying movement on the ball
				FVector CarryVector = GetActorLocation() + (GetActorForwardVector() * 30.0f) - Ball->GetActorLocation();
				CarryVector.Set(CarryVector.X/2, CarryVector.Y/2, 0.0f);
				CarryVector += GetActorRightVector() * RightInput * CarryVelocityMultiplier;
				CarryVector *= CarryVelocityMultiplier;

				Ball->Move(CarryVector);
				MovementCooldownDelta = MovementCooldown / 2.0f;
			}			
		}
	}

	//* only allow continuous carry of the ball when near stationary */
	if (Velocity.Size() > 300.0f) {
		CarryBallInput = false;
	}
}

void AAuthFootballCharacter::StartRun() 
{
	Running = true;

	//* call the Run command on the server if we are a client */
	if (GetLocalRole() != ROLE_Authority) {
		Server_Run(true);
	}
}

void AAuthFootballCharacter::StopRun() 
{
	Running = false;

	//* call the Run command on the server if we are a client */
	if (GetLocalRole() != ROLE_Authority) {
		Server_Run(false);
	}
}

void AAuthFootballCharacter::StartCarryBall()
{
	CarryBallInput = true;

	//* call the CarryBall command on the server if we are a client */
	if (GetLocalRole() != ROLE_Authority) {
		Server_CarryBall(true);
	}
}

void AAuthFootballCharacter::StopCarryBall()
{
	CarryBallInput = false;

	//* call the CarryBall command on the server if we are a client */
	if (GetLocalRole() != ROLE_Authority) {
		Server_CarryBall(false);
	}
}

void AAuthFootballCharacter::HoldPowerUp() 
{
	ShootBallInput = true;
}

void AAuthFootballCharacter::ReleasePowerUp()
{
	ShootBall();

	//* call the ShootBall command on the server if we are a client */
	if (GetLocalRole() != ROLE_Authority) {
		Server_ShootBall(HoldPowerForce);
	}

	ShootBallInput = false;
	HoldPowerForce = 0.0f;
}

bool AAuthFootballCharacter::CheckForMovementCooldown() {
	if (MovementCooldownDelta >= MovementCooldown) {
		MovementCooldownDelta = 0.0f;
		return true;
	}
	return false;
}

void AAuthFootballCharacter::MoveForward(float Value)
{
	if (Value != 0.0f) {
		AddMovementInput(GetActorForwardVector(), Value);
	}
	ForwardInput = Value;

	//* call the MoveForward command on the server if we are a client */
	if (GetLocalRole() != ROLE_Authority) {
		Server_MoveForward(Value);
	}
}

void AAuthFootballCharacter::MoveRight(float Value)
{
	if (Value != 0.0f) {
		AddMovementInput(GetActorRightVector(), Value);
	}
	RightInput = Value;

	//* call the MoveForward command on the server if we are a client */
	if (GetLocalRole() != ROLE_Authority) {
		Server_MoveRight(Value);
	}
}

void AAuthFootballCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AAuthFootballCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}



/////////////* Server RPCs */
void AAuthFootballCharacter::Server_Run_Implementation(bool start)
{
	Running = start;
}

void AAuthFootballCharacter::Server_MoveForward_Implementation(float Value)
{
	ForwardInput = Value;
}

void AAuthFootballCharacter::Server_MoveRight_Implementation(float Value)
{
	RightInput = Value;
}

void AAuthFootballCharacter::Server_CarryBall_Implementation(bool start) {
	UE_LOG(LogTemp, Warning, TEXT("Server Recieved carry command"));
	CarryBallInput = start;
}

void AAuthFootballCharacter::Server_ShootBall_Implementation(float _HoldPowerForce)
{
	HoldPowerForce = _HoldPowerForce;
	ShootBall();
}

/////////////* Server RPCs Validation */
bool AAuthFootballCharacter::Server_Run_Validate(bool start)
{
	return true;
}

bool AAuthFootballCharacter::Server_MoveForward_Validate(float Value)
{
	return ValidateInput(Value);
}

bool AAuthFootballCharacter::Server_MoveRight_Validate(float Value)
{
	return ValidateInput(Value);
}

bool AAuthFootballCharacter::ValidateInput(float Value) {
	if (FMath::Abs(Value) > 1.0f) {
		UE_LOG(LogTemp, Warning, TEXT("Invalid input from Client"));
		return false;
	}
	return true;
}

bool AAuthFootballCharacter::Server_CarryBall_Validate(bool start) {	
	return true;
}

bool AAuthFootballCharacter::Server_ShootBall_Validate(float _HoldPowerForce) {
	if (_HoldPowerForce > HoldPowerLimit + HoldPowerInputMultiplier) {
		UE_LOG(LogTemp, Warning, TEXT("Hold power was above the limit: %f"), _HoldPowerForce);
		return false;
	}
	return true;
}

/*
// try and play the sound if specified
if (FireSound != NULL)
{
	UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
}

// try and play a firing animation if specified
if (FireAnimation != NULL)
{
	// Get the animation object for the arms mesh
	UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
	if (AnimInstance != NULL)
	{
		AnimInstance->Montage_Play(FireAnimation, 1.f);
	}
}*/
