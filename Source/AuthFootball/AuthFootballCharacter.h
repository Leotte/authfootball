// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Online.h"
#include "Ball.h"
#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AuthFootballCharacter.generated.h"

class UInputComponent;

UCLASS(config=Game)
class AAuthFootballCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

public:
	AAuthFootballCharacter();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=GamePlay)
	class ABall* Ball;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay|Movement")
	float WalkingSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay|Movement")
	float RunningSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay|Movement")
	bool Running;

	UPROPERTY(Transient, BlueprintReadWrite, Category = "Gameplay|Movement")
	FVector Velocity;

	//*Player actions */
	UPROPERTY(EditAnywhere, Category = "Gameplay|Forces")
	int ShootForce;
	UPROPERTY(EditAnywhere, Category = "Gameplay|Forces")
	int CarryVelocityMultiplier;
	UPROPERTY(EditAnywhere, Category = "Gameplay|Forces")
	int RunningVelocityMultiplier;
	UPROPERTY(EditAnywhere, Category = "Gameplay|Forces")
	int HoldPowerLimit;
	UPROPERTY(EditAnywhere, Category = "Gameplay|Forces")
	float HoldPowerInputMultiplier;
	UPROPERTY(EditAnywhere, Category = "Gameplay|Forces")
	float MaxDistanceToBall;
	UPROPERTY(EditAnywhere, Category = "Gameplay|Forces")
	float MovementCooldown;

	float HoldPowerForce = 0;
	bool ShootBallInput = false;
	bool CanCarryBall = false, CarryBallInput = false;
	float ForwardInput = 0.0f;
	float RightInput = 0.0f;
	float MovementCooldownDelta = 0.0f;

	/** Sound to play each time we fire */
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	//class USoundBase* FireSound;

	/** AnimMontage to play each time we fire 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	class UAnimMontage* FireAnimation;*/

protected:

	void HoldPowerUp();
	void ReleasePowerUp();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_CarryBall(bool start);
	void StartCarryBall();
	void StopCarryBall();

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_ShootBall(float _HoldPowerForce);
	void ShootBall();
	void CarryBall();
	bool CheckForMovementCooldown();

	/** Handles moving forward/backward */
	void MoveForward(float Val);
	UFUNCTION(Server, Unreliable, WithValidation)
	void Server_MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);
	UFUNCTION(Server, Unreliable, WithValidation)
	void Server_MoveRight(float Val);

	//*Handles running */
	void StartRun();
	void StopRun();
	UFUNCTION(Server, Reliable, WithValidation)
	void Server_Run(bool start);

	bool ValidateInput(float Value);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	void GetSteamID();


	UPROPERTY(Transient, VisibleAnywhere, BlueprintReadOnly, Category = Network)
	FString SteamID;
	TSharedPtr<const FUniqueNetId> PlayerID;
};

